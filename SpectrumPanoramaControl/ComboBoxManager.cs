﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace SpectrumPanoramaControl
{
    public partial class SPControl
    {
        public enum CorrelationVisibility
        {
            True,
            False
        }

        int index = 4;
        public int Index 
        {
            get => index;
            set 
            {
                index = value;
                cbPanoramas.SelectedIndex = index;
                OnSendCBIndex?.Invoke(index);
            }
        }

        int indexCorr = 1;
        public int IndexCorr 
        {
            get => indexCorr;
            set 
            {
                indexCorr = value;
                //cbCorrPanoramas.SelectedIndex = indexCorr;
                
                OnSendCBCorrIndex?.Invoke(indexCorr);
            }
        }

        public delegate void SendComboBoxIndex(int count);
        public event SendComboBoxIndex OnSendCBIndex = (count) => { };
        public event SendComboBoxIndex OnSendCBCorrIndex = (count) => { };

        public delegate void CorrPanoramaVisibility(CorrelationVisibility correlationVisibility);
        public event CorrPanoramaVisibility OnCorrelationVisibility = (visibility) => { };

        private void cbPanoramas_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (cbPanoramas.SelectedIndex) 
            {
                case 0:
                    OnSendCBIndex?.Invoke(0);
                    break;
                case 1:
                    OnSendCBIndex?.Invoke(1);
                    break;
                case 2:
                    OnSendCBIndex?.Invoke(2);
                    break;
                case 3:
                    OnSendCBIndex?.Invoke(3);
                    break;
                case 4:
                    OnSendCBIndex?.Invoke(4);
                    break;
                case 5:
                    OnSendCBIndex?.Invoke(5);
                    break;
            }
        }

        private void cbCorrPanoramas_SelectionChanged(object sender, SelectionChangedEventArgs e) 
        {
            switch (cbCorrPanoramas.SelectedIndex) 
            {
                case 0:
                    OnSendCBCorrIndex?.Invoke(0);
                    break;
                case 1:
                    OnSendCBCorrIndex?.Invoke(1);
                    break;
                case 2:
                    OnSendCBCorrIndex?.Invoke(2);
                    break;
            }
        }

        private void BCorrPanorama_Unchecked(object sender, RoutedEventArgs e)
        {
            OnCorrelationVisibility?.Invoke(CorrelationVisibility.False);
        }

        private void BCorrPanorama_Checked(object sender, RoutedEventArgs e)
        {
            OnCorrelationVisibility?.Invoke(CorrelationVisibility.True);
        }

        public void CorrPanoramaIsVisibility(CorrelationVisibility correlationVisibility)
        {
            switch (correlationVisibility)
            {
                case CorrelationVisibility.True:
                    BCorrPanorama.IsChecked = true;
                    break;
                case CorrelationVisibility.False:
                    BCorrPanorama.IsChecked = false;
                    break;
                default:
                    break;
            }            
        }
    }
}
