﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace SpectrumPanoramaControl
{
    class Translations : INotifyPropertyChanged
    {
        private string _MHz;
        private string _dB;

        public Translations()
        {
            this._MHz = "МГц";
            this._dB = "дБ";
        }

        public string MHz
        {
            get { return _MHz; }
            set
            {
                _MHz = value;
                OnPropertyChanged("MHz");
            }
        }
        public string dB
        {
            get { return _dB; }
            set
            {
                _dB = value;
                OnPropertyChanged("dB");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
