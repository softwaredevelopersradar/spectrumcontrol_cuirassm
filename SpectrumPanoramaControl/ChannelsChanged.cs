﻿using System.Windows;
using System.Windows.Controls;

namespace SpectrumPanoramaControl
{
    public partial class SPControl : UserControl
    {
        //ВЫБОР КАНАЛОВ СЧИТЫВАНИЯ ДАННЫХ

        public delegate void SendChannelStatus(object sender, byte id, bool status);
        public event SendChannelStatus OnSendChannelStatus = (sender, id, status) => { };

        public void DefaultChannels(bool channel1, bool channel2, bool channel3, bool channel4)
        {
            bChOne.IsChecked = channel1;
            bChTwo.IsChecked = channel2;
            bChThree.IsChecked = channel3;
            bChFour.IsChecked = channel4;
        }

        private void BChOne_Checked(object sender, RoutedEventArgs e)
        {
            pointSeries.Visible = true;
            OnSendChannelStatus?.Invoke(this, 1, true);            
        }

        private void BChOne_Unchecked(object sender, RoutedEventArgs e)
        {
            pointSeries.Visible = false;
            OnSendChannelStatus?.Invoke(this, 1, false);            
        }

        private void BChTwo_Checked(object sender, RoutedEventArgs e)
        {
            pointSeries2.Visible = true;
            OnSendChannelStatus?.Invoke(this, 2, true);            
        }

        private void BChTwo_Unchecked(object sender, RoutedEventArgs e)
        {
            pointSeries2.Visible = false;
            OnSendChannelStatus?.Invoke(this, 2, false);            
        }

        private void BChThree_Checked(object sender, RoutedEventArgs e)
        {
            pointSeries3.Visible = true;
            OnSendChannelStatus?.Invoke(this, 3, true);            
        }

        private void BChThree_Unchecked(object sender, RoutedEventArgs e)
        {
            pointSeries3.Visible = false;
            OnSendChannelStatus?.Invoke(this, 3, false);            
        }

        private void BChFour_Checked(object sender, RoutedEventArgs e)
        {
            pointSeries4.Visible = true;
            OnSendChannelStatus?.Invoke(this, 4, true);
        }

        private void BChFour_Unchecked(object sender, RoutedEventArgs e)
        {
            pointSeries4.Visible = false;
            OnSendChannelStatus?.Invoke(this, 4, false);
        }

        private void BChSum_Checked(object sender, RoutedEventArgs e)
        {
            pointSeries5.Visible = true;
            OnSendChannelStatus?.Invoke(this, 5, true);
        }

        private void BChSum_Unchecked(object sender, RoutedEventArgs e)
        {
            pointSeries5.Visible = false;
            OnSendChannelStatus?.Invoke(this, 5, false);
        }

        private void BChMd_Checked(object sender, RoutedEventArgs e)
        {
            pointSeries6.Visible = true;
            OnSendChannelStatus?.Invoke(this, 6, true);
        }

        private void BChMd_Unchecked(object sender, RoutedEventArgs e)
        {
            pointSeries6.Visible = false;
            OnSendChannelStatus?.Invoke(this, 6, false);
        }

        private void BChSm_Checked(object sender, RoutedEventArgs e)
        {
            pointSeries7.Visible = true;
            OnSendChannelStatus?.Invoke(this, 7, true);
        }

        private void BChSm_Unchecked(object sender, RoutedEventArgs e)
        {
            pointSeries7.Visible = false;
            OnSendChannelStatus?.Invoke(this, 7, false);
        }
    }
}
