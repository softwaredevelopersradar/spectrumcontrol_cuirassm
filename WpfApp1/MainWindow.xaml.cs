﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            spControl.StorageChange += SpControl_StorageChange;

            spControl.BandOnRSEvent += SpControl_BandOnRSEvent;

            spControl.ThresholdChange += SpControl_ThresholdChange;

            InitColorDialog();
        }

        private void SpControl_ThresholdChange(int value)
        {
            Console.WriteLine(value);
        }

        private void SpControl_BandOnRSEvent(double freqLeftMHz, double freqRightMHz)
        {
            Console.Beep();
        }

        System.Windows.Forms.ColorDialog colorDlg = new System.Windows.Forms.ColorDialog();
        private void InitColorDialog()
        {
            colorDlg.AllowFullOpen = true;
            colorDlg.FullOpen = true;
            colorDlg.AnyColor = true;
            colorDlg.SolidColorOnly = false;
        }

        private void SpControl_StorageChange(bool value)
        {
            //Console.WriteLine(value);
        }

        private void one_Click(object sender, RoutedEventArgs e)
        {
            Random random = new Random();
            spControl.CursorColor = Color.FromArgb(255, (byte)random.Next(), (byte)random.Next(255), (byte)random.Next(255));
        }

        private void two_Click(object sender, RoutedEventArgs e)
        {
            //ColorPicker colorPicker = new ColorPicker();
            if (colorDlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                spControl.RRLineColor = Color.FromArgb(colorDlg.Color.A, colorDlg.Color.R, colorDlg.Color.G, colorDlg.Color.B);
            }
            //Random random = new Random();
            //spControl.RRLineColor = Color.FromArgb(255, (byte)random.Next(), (byte)random.Next(255), (byte)random.Next(255));
        }

        private void three_Click(object sender, RoutedEventArgs e)
        {
            Random random = new Random();
            spControl.TitleColor = Color.FromArgb(255, (byte)random.Next(), (byte)random.Next(255), (byte)random.Next(255));
        }

        private void four_Click(object sender, RoutedEventArgs e)
        {
            Random random = new Random();
            spControl.AxisColor = Color.FromArgb(255, (byte)random.Next(), (byte)random.Next(255), (byte)random.Next(255));
        }

        private void five_Click(object sender, RoutedEventArgs e)
        {
            Random random = new Random();
            spControl.LabelsColor = Color.FromArgb(255, (byte)random.Next(), (byte)random.Next(255), (byte)random.Next(255));
        }

        private void six_Click(object sender, RoutedEventArgs e)
        {
            Random random = new Random();
            spControl.BackGround = Color.FromArgb(255, (byte)random.Next(), (byte)random.Next(255), (byte)random.Next(255));
        }

        private void seven_Click(object sender, RoutedEventArgs e)
        {
            Random random = new Random();
            spControl.SpectrumColor = Color.FromArgb(255, (byte)random.Next(), (byte)random.Next(255), (byte)random.Next(255));
        }

        private void eight_Click(object sender, RoutedEventArgs e)
        {
            Random random = new Random();
            double[] data = new double[2000];
            for (int i=0; i< 2000; i++)
            {
                data[i] = random.NextDouble() * -120;
            }
            //spControl.PlotSpectr(25,3025,data);

        }

        private void nine_Click(object sender, RoutedEventArgs e)
        {
            double [] xdata = new double[3];
            double [] ydata = new double[3];

            xdata[0] = 35;
            xdata[1] = 40;
            xdata[2] = 45;

            ydata[0] = 0;
            ydata[1] = -10;
            ydata[2] = -20;

            spControl.DrawRSArrows(true, xdata, ydata);

        }


        double[] temp;
        double temp1;
        double temp2;

        private void ten_Click(object sender, RoutedEventArgs e)
        {
            Random random = new Random();
            int N = 10;
            double[] data = new double[N];
            for (int i = 0; i < data.Count(); i++)
            {
                data[i] = random.NextDouble() * -120;
                if (data[i] > -90) data[i] = -95;
                if (data[i] < -100) data[i] = -95;
            }

            data[random.Next(0, N)] = -50;
            data[random.Next(0, N)] = -50;

            data[random.Next(0, N)] = -120;
            data[random.Next(0, N)] = -120;


            //spControl.PlotSpectr(spControl.MinVisibleX, spControl.MaxVisibleX, data);

            //temp = data;
            //temp1 = spControl.MinVisibleX;
            //temp2 = spControl.MaxVisibleX;

            //spControl.PlotSaveStorage(spControl.MinVisibleX, spControl.MaxVisibleX, data);


            //double[] data = new double[5];
            //data[0] = -90;
            //data[1] = -60;
            //data[2] = -90;
            //data[3] = -90;
            //data[4] = -90;

            //spControl.PlotSpectr(spControl.MinVisibleX, spControl.MaxVisibleX, data);

            //spControl.PlotSaveStorage(spControl.MinVisibleX, spControl.MaxVisibleX, data);
        }

        private void eleven_Click(object sender, RoutedEventArgs e)
        {
            //spControl.PlotStorage(temp1, temp2, temp);
            //spControl.PlotStorage2(temp1, temp2, temp);

            //spControl.PlotStorage(temp1, temp2, temp);

            //spControl.PlotStorageV1(temp1, temp2, temp);

            //spControl.PlotStorageV2(temp1, temp2, temp);

            //temp = new[] { -90d, -91d, -95d, -97d, -80d, -79d, -78d, -75d, -76d, -77d, -79d, -80d, -90d, - 95d };

            //spControl.PlotStorageV3(temp1, temp2, temp);



            Random random = new Random();
            int N = 3000;
            double[] data = new double[N];
            for (int i = 0; i < data.Count(); i++)
            {
                if (i < data.Count() / 2)
                {
                    data[i] = random.NextDouble() * -120;
                    if (data[i] > -90) data[i] = -95;
                    if (data[i] < -100) data[i] = -95;
                }
                else
                {
                    data[i] = -130;
                }
            }

            //spControl.PlotSpectr(spControl.MinVisibleX, spControl.MaxVisibleX, data);

            //spControl.PlotSaveStorage(spControl.MinVisibleX, spControl.MaxVisibleX, data);


            //double[] data = new double[5];
            //data[0] = -90;
            //data[1] = -90;
            //data[2] = -60;
            //data[3] = -90;
            //data[4] = -90;

            //spControl.PlotSpectr(spControl.MinVisibleX, spControl.MaxVisibleX, data);

            //spControl.PlotSaveStorage(spControl.MinVisibleX, spControl.MaxVisibleX, data);

        }

        private void twelve_Click(object sender, RoutedEventArgs e)
        {



            spControl.ClearStorage();
        }

        private void thirteen_Click(object sender, RoutedEventArgs e)
        {
            spControl.xRangeLabelVisible = !spControl.xRangeLabelVisible;
        }

        private void fourteen_Click(object sender, RoutedEventArgs e)
        {
            spControl.xAxisLabelVisible = !spControl.xAxisLabelVisible;
        }

        private void fifthteen_Click(object sender, RoutedEventArgs e)
        {
            spControl.yAxisLabelVisible = !spControl.yAxisLabelVisible;
        }

        private void sixteen_Click(object sender, RoutedEventArgs e)
        {
            Random r = new Random();
            spControl.AdaptiveThreshold = r.Next(-110, -90);
        }

        bool flag = false;
        private void seventeen_Click(object sender, RoutedEventArgs e)
        {
            flag = !flag;
            spControl.ScanSpeedVisible = flag;
        }

        private void eighteen_Click(object sender, RoutedEventArgs e)
        {
            //spControl.GlobalRangeXmin = 100;

            byte[] Spectrum = new byte[256];
            for (int i = 0; i < Spectrum.Count(); i++)
            {
                Spectrum[i] = (byte)i;
            }

            int MinSpectrValue = -130;
            double[] data = new double[Spectrum.Count()];
            for (int i = 0; i < Spectrum.Count(); i++)
            {
                data[i] = MinSpectrValue + Spectrum[i];
            }
            Console.Beep();
        }

        private void nineteen_Click(object sender, RoutedEventArgs e)
        {

            //byte[] Bands = new byte[100];
            //for (int i = 0; i < Bands.Count(); i++)
            //{
            //    Bands[i] = (byte)i;
            //}

            byte[] Bands = new byte [] { 0, 2, 4, 6, 7, 10, 11, 13 };

            byte[] Thresholds = new byte[Bands.Count()];
            Random random = new Random();
            for (int i = 0; i < Thresholds.Count(); i++)
            {
                Thresholds[i] = (byte)random.Next(85, 89);
            }

            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            spControl.AdaptiveBandsPaint(Bands, Thresholds);

            stopWatch.Stop();

            // Get the elapsed time as a TimeSpan value.
            TimeSpan ts = stopWatch.Elapsed;

            // Format and display the TimeSpan value.
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                ts.Hours, ts.Minutes, ts.Seconds,
                ts.Milliseconds);
            Console.WriteLine("RunTime " + elapsedTime);

        }

        private void twenty_Click0(object sender, RoutedEventArgs e)
        {
            //spControl.ExternalExBearing(50, 10);



            List<double> lFreqStart = new List<double>();
            List<double> lFreqEnd = new List<double>();
            List<double[]> lCutOffFreq = new List<double[]>();
            List<double[]> lCutOffWidth = new List<double[]>();
            List<double> lThreshold = new List<double>();

            for (int i = 0; i < 1; i++)
            {
                lFreqStart.Add(30);
                lFreqEnd.Add(90);

                double[] templCutOffFreq = new double[1];
                double[] templCutOffWidth = new double[1];

                templCutOffFreq[0] = 60;
                templCutOffWidth[0] = 10;

                lCutOffFreq.Add(templCutOffFreq);
                lCutOffWidth.Add(templCutOffWidth);

                lThreshold.Add(-60);
            }

            for (int i = 0; i < 1; i++)
            {
                lFreqStart.Add(100);
                lFreqEnd.Add(160);

                double[] templCutOffFreq = new double[1];
                double[] templCutOffWidth = new double[1];

                templCutOffFreq[0] = 120;
                templCutOffWidth[0] = 10;

                lCutOffFreq.Add(templCutOffFreq);
                lCutOffWidth.Add(templCutOffWidth);

                lThreshold.Add(-40);
            }

            spControl.ClearFHSSonRS();
            spControl.FHSSonRS(lFreqStart, lFreqEnd, lCutOffFreq, lCutOffWidth, lThreshold);
        }

        private void twenty_Click(object sender, RoutedEventArgs e)
        {
            //spControl.ExternalExBearing(50, 10);



            List<double> lFreqStart = new List<double>();
            List<double> lFreqEnd = new List<double>();
            List<double[]> lCutOffFreq = new List<double[]>();
            List<double[]> lCutOffWidth = new List<double[]>();
            List<double> lThreshold = new List<double>();

            for (int i = 0; i < 1; i++)
            {
                lFreqStart.Add(30);
                lFreqEnd.Add(50);

                double[] templCutOffFreq = new double[0];
                double[] templCutOffWidth = new double[0];

                lCutOffFreq.Add(templCutOffFreq);
                lCutOffWidth.Add(templCutOffWidth);

                lThreshold.Add(-60);
            }

            for (int i = 0; i < 1; i++)
            {
                lFreqStart.Add(70);
                lFreqEnd.Add(100);

                double[] templCutOffFreq = new double[0];
                double[] templCutOffWidth = new double[0];

                lCutOffFreq.Add(templCutOffFreq);
                lCutOffWidth.Add(templCutOffWidth);

                lThreshold.Add(-60);
            }

            spControl.ClearFHSSonRS();
            spControl.FHSSonRS(lFreqStart, lFreqEnd, lCutOffFreq, lCutOffWidth, lThreshold);



            byte[] b = new byte[10];
            byte[] t = new byte[10];
            for (int i = 0; i < b.Count(); i++)
            {
                b[i] = (byte)i;
                t[i] = (byte)80;
            }

            spControl.AdaptiveBandsPaint(b, t);
        }

        private void twentyone_Click(object sender, RoutedEventArgs e)
        {
            double[] xData = new double[100];

            for (int i = 0; i < xData.Count(); i++)
            {
                xData[i] = 25d + (175d / 100d * i);
            }

            spControl.DrawRSLines2(xData, -80);
        }

        private void twentytwo_Click(object sender, RoutedEventArgs e)
        {
            spControl.FHSSonRSVisible = !spControl.FHSSonRSVisible;


            List<int> lint = new List<int>();
            lint.Add(0);
            lint.Add(1);
            lint.Add(6);
            lint.Add(7);
            lint.Insert(2, 2);
            lint.Insert(3,3);
            lint.Insert(4, 4);
            lint.Insert(5, 5);

        }

        private void twentythree_Click(object sender, RoutedEventArgs e)
        {
            List<double[]> lxData = new List<double[]>();
            List<double> lyLevel = new List<double>();

            Random random = new Random();

            for (int w = 0; w < 4; w++)
            {
                double[] xData = new double[10];
                for (int i = 0; i < 10; i++)
                {
                    xData[i] = random.NextDouble() * 10 + (w * 10 + 30);
                }
                lxData.Add(xData);
                lyLevel.Add(-80 + w * 10);
            }
            spControl.ClearArrowsAndLines();
            spControl.DrawRSLines(lxData,lyLevel);
        }

        private void twentyfour_Click(object sender, RoutedEventArgs e)
        {
            var vColors = YamlLoad();

            spControl.SpectrumColor = Color.FromArgb(255, (byte)vColors.allColors.SpectrumColor.R , (byte)vColors.allColors.SpectrumColor.G, (byte)vColors.allColors.SpectrumColor.B);

            spControl.RRLineColor = Color.FromArgb(255, (byte)vColors.allColors.ThresholdColor.R, (byte)vColors.allColors.ThresholdColor.G, (byte)vColors.allColors.ThresholdColor.B);
            spControl.CursorColor = Color.FromArgb(255, (byte)vColors.allColors.CursorColor.R, (byte)vColors.allColors.CursorColor.G, (byte)vColors.allColors.CursorColor.B);
            spControl.BackGround = Color.FromArgb(255, (byte)vColors.allColors.BackGroudColor.R, (byte)vColors.allColors.BackGroudColor.G, (byte)vColors.allColors.BackGroudColor.B);

            spControl.AxisColor = Color.FromArgb(255, (byte)vColors.allColors.MarkupColor.R, (byte)vColors.allColors.MarkupColor.G, (byte)vColors.allColors.MarkupColor.B);
            spControl.LabelsColor = Color.FromArgb(255, (byte)vColors.allColors.MarkupColor.R, (byte)vColors.allColors.MarkupColor.G, (byte)vColors.allColors.MarkupColor.B);
        }

        private void twentyfive_Click(object sender, RoutedEventArgs e)
        {
            int N = 10000;

            TimeSpan t = new TimeSpan();

            for (int w = 0; w < N; w++)
            {
                Random random = new Random();
                double[] data = new double[3000];
                for (int i = 0; i < 3000; i++)
                {
                    data[i] = random.NextDouble() * -120;
                }

                Stopwatch stopWatch = new Stopwatch();
                stopWatch.Start();
               
                //spControl.PlotSpectr(25, 3025, data);

                stopWatch.Stop();

                t += stopWatch.Elapsed;
            }

            string elapsedTime = String.Format("{0:00}:{1:00}.{2:000}", t.Minutes, t.Seconds, t.Milliseconds);
            Console.WriteLine("RunTime " + elapsedTime);
        }

        private void thirtyone_Click(object sender, RoutedEventArgs e)
        {
            double[] FreqsStartMHz = new double[2];
            double[] FreqsEndMHz = new double[2];

            FreqsStartMHz[0] = 30;
            FreqsEndMHz[0] = 40;
            FreqsStartMHz[1] = 50;
            FreqsEndMHz[1] = 60;

            spControl.PaintSpecBands(2, FreqsStartMHz, FreqsEndMHz);
        }

        private void thirtytwo_Click(object sender, RoutedEventArgs e)
        {
            double[] FreqsStartMHz = new double[2];
            double[] FreqsEndMHz = new double[2];

            FreqsStartMHz[0] = 70;
            FreqsEndMHz[0] = 80;
            FreqsStartMHz[1] = 90;
            FreqsEndMHz[1] = 100;

            spControl.PaintSpecBands(0, FreqsStartMHz, FreqsEndMHz);
        }

        private void thirtythree_Click(object sender, RoutedEventArgs e)
        {
            double[] FreqsStartMHz = new double[2];
            double[] FreqsEndMHz = new double[2];

            FreqsStartMHz[0] = 110;
            FreqsEndMHz[0] = 120;
            FreqsStartMHz[1] = 130;
            FreqsEndMHz[1] = 140;

            spControl.PaintSpecBands(1, FreqsStartMHz, FreqsEndMHz);
        }

        private void thirtyfour_Click(object sender, RoutedEventArgs e)
        {
           
        }

        private void thirtyfive_Click(object sender, RoutedEventArgs e)
        {
            Random r = new Random();
            double d = r.NextDouble() * 3000;
            spControl.CursorCRRChange(d, 0.3d);
        }

        private void thirtysix_Click(object sender, RoutedEventArgs e)
        {
            spControl.SetLanguage("rus");
        }

        private void thirtyseven_Click(object sender, RoutedEventArgs e)
        {
            spControl.SetLanguage("eng");
        }

        private void thirtyeight_Click(object sender, RoutedEventArgs e)
        {

        }

        private void thirtynine_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
